﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace HCI.Class
{

    [XmlRoot("movie")]
    public class Actor: INotifyPropertyChanged
    {
        private String _actorName = null;

        [XmlElement("actor")]
        public String ActorName 
        {
            get { return _actorName; }
            set
            {
                _actorName = value;
                OnPropertyChanged("ActorName");
            }
        }

        public Actor() 
        {
            //empty
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(String name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }
}
