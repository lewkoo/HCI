﻿namespace HCI
{
    partial class LogIn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.reqOne = new System.Windows.Forms.TextBox();
            this.reqTwo = new System.Windows.Forms.TextBox();
            this.usrName = new System.Windows.Forms.TextBox();
            this.pWord = new System.Windows.Forms.TextBox();
            this.userName = new System.Windows.Forms.TextBox();
            this.password = new System.Windows.Forms.TextBox();
            this.login = new System.Windows.Forms.Button();
            this.register = new System.Windows.Forms.Button();
            this.exitButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.reqOne, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.reqTwo, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.usrName, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.pWord, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.userName, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.password, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.login, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.register, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.exitButton, 1, 3);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(1, 2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(383, 148);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // reqOne
            // 
            this.reqOne.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.reqOne.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.reqOne.Enabled = false;
            this.reqOne.ForeColor = System.Drawing.Color.Red;
            this.reqOne.Location = new System.Drawing.Point(193, 58);
            this.reqOne.Name = "reqOne";
            this.reqOne.Size = new System.Drawing.Size(108, 13);
            this.reqOne.TabIndex = 2;
            this.reqOne.Text = "* required";
            // 
            // reqTwo
            // 
            this.reqTwo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.reqTwo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.reqTwo.Enabled = false;
            this.reqTwo.ForeColor = System.Drawing.Color.Red;
            this.reqTwo.Location = new System.Drawing.Point(193, 95);
            this.reqTwo.Name = "reqTwo";
            this.reqTwo.Size = new System.Drawing.Size(108, 13);
            this.reqTwo.TabIndex = 3;
            this.reqTwo.Text = "* required";
            // 
            // usrName
            // 
            this.usrName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.usrName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.usrName.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.usrName.Location = new System.Drawing.Point(79, 58);
            this.usrName.Name = "usrName";
            this.usrName.Size = new System.Drawing.Size(108, 13);
            this.usrName.TabIndex = 4;
            this.usrName.Text = "\r\n";
            // 
            // pWord
            // 
            this.pWord.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pWord.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.pWord.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pWord.Location = new System.Drawing.Point(79, 95);
            this.pWord.Name = "pWord";
            this.pWord.Size = new System.Drawing.Size(108, 13);
            this.pWord.TabIndex = 5;
            // 
            // userName
            // 
            this.userName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.userName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.userName.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.userName.Location = new System.Drawing.Point(3, 58);
            this.userName.Name = "userName";
            this.userName.Size = new System.Drawing.Size(70, 13);
            this.userName.TabIndex = 7;
            this.userName.Text = "User Name:";
            // 
            // password
            // 
            this.password.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.password.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.password.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.password.Location = new System.Drawing.Point(3, 95);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(70, 13);
            this.password.TabIndex = 8;
            this.password.Text = "Password:";
            // 
            // login
            // 
            this.login.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.login.Location = new System.Drawing.Point(3, 122);
            this.login.Name = "login";
            this.login.Size = new System.Drawing.Size(70, 23);
            this.login.TabIndex = 9;
            this.login.Text = "Login";
            this.login.UseVisualStyleBackColor = true;
            this.login.Click += new System.EventHandler(this.login_Click);
            // 
            // register
            // 
            this.register.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.register.Location = new System.Drawing.Point(231, 122);
            this.register.Name = "register";
            this.register.Size = new System.Drawing.Size(70, 23);
            this.register.TabIndex = 10;
            this.register.Text = "Register";
            this.register.UseVisualStyleBackColor = true;
            this.register.Click += new System.EventHandler(this.register_Click_1);
            // 
            // exitButton
            // 
            this.exitButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.exitButton.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.exitButton.Location = new System.Drawing.Point(79, 122);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(75, 23);
            this.exitButton.TabIndex = 11;
            this.exitButton.Text = "Return";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 152);
            this.ControlBox = false;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Form2";
            this.Text = "Log In/Register Window";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox reqOne;
        private System.Windows.Forms.TextBox reqTwo;
        private System.Windows.Forms.TextBox usrName;
        private System.Windows.Forms.TextBox pWord;
        private System.Windows.Forms.TextBox userName;
        private System.Windows.Forms.TextBox password;
        private System.Windows.Forms.Button login;
        private System.Windows.Forms.Button register;
        private System.Windows.Forms.Button exitButton;
    }
}