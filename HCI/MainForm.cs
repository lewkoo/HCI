﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using HCI.Class;
using System.Windows;
using System.IO;
using System.Configuration;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
using System.Xml;
using System.Diagnostics;
using HCI.Properties;

namespace HCI
{
    public partial class MainForm : Form
    {
        #region Variables

        MovieSearch addMovie;
        LogIn logIn;
        User currentUser = null;

        string moviesSource = Path.Combine(Directory.GetCurrentDirectory() + "\\movies.xml");
        string usersSource = Path.Combine(Directory.GetCurrentDirectory() + "\\users.xml");
        [XmlElement("movieList", typeof(MovieList))]
        private MovieList _movies = new MovieList();
        public MovieList Movies
        {
            get { return _movies; }
            set
            {
                if (value != null)
                {
                    _movies = value;
                    OnPropertyChanged("Movies");
                }
            }
        }

        [XmlElement("userList", typeof(UserList))]
        private UserList _users = new UserList();
        public UserList Users
        {
            get { return _users; }
            set
            {
                if (value != null)
                {
                    _users = value;
                    OnPropertyChanged("Users");
                }

            }
        }

        #endregion
        //accessors/mutators END

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }



        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            SerializeToFile(Movies, moviesSource);
            SerializeToFile(Users, usersSource);
        }
        #region XmlMethods

        private void GetMovies()
        {
            Movies = (MovieList)DeserializeXML("movielist", typeof(MovieList), moviesSource);
        }

        private void GetUsers()
        {
            Users = (UserList)DeserializeXML("userlist", typeof(UserList), usersSource);
        }

        private object DeserializeXML(string elementName, Type type, string fileLocation)
        {
            XmlRootAttribute xRoot = new XmlRootAttribute();
            xRoot.ElementName = elementName;
            XmlSerializer serializer = new XmlSerializer(type, xRoot);

            XmlReader reader = XmlReader.Create(fileLocation);
            var toReturn = serializer.Deserialize(reader);
            reader.Close();
            return toReturn;

        }

        private void SerializeToFile(object toSerialize, string location)
        {
            using (StreamWriter stream = new StreamWriter(location))
            {
                XmlSerializer serializer = new XmlSerializer(toSerialize.GetType());
                serializer.Serialize(stream, toSerialize);
            }
        }

        #endregion


        public MainForm()
        {
            InitializeComponent();
            logIn = new LogIn();
            GetMovies();
            GetUsers();
            LoadRecentMovies();
            showButton.Hide();
            addMovie = new MovieSearch();
        }

        

        #region ActionMethods

        private void logButton_Click(object sender, EventArgs e)
        {
            if (currentUser != null)
            {
                //log out action
                currentUser = null;
                logButton.Text = "Log in";
                userBox.Text = "No user logged in";

            }
            else
            {

                logIn = new LogIn(Users);
                //logIn.Show();
                var dialogResult = logIn.ShowDialog();
                logButton.Text = "Log out";
                if (dialogResult != System.Windows.Forms.DialogResult.Cancel)
                {
                    currentUser = logIn.SelectedUser;
                    userBox.Text = logIn.SelectedUser.getUserName();

                    if (logIn.UserRegistered == true) //if we have a new user/users registered
                        Users = logIn.GetModifiedUserList;
                }
            }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            //moviesSearchedList.Items.Add(addMovie);
            //moviesSearchedList.Refresh();
        }

        private void hideButton_Click(object sender, EventArgs e)
        {
            tabsControls.Hide();
            hideButton.Hide();
            showButton.Show();
        }

        private void showButton_Click(object sender, EventArgs e)
        {
            tabsControls.Show();
            hideButton.Show();
            showButton.Hide();
        }

        private void quitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        #region StartUpRoutines

        private void LoadRecentMovies()
        {


            for(int i = 0; i < 7; i++)
            flowLayoutPanel2.Controls.Add(new MovieDisplay());



        }

        #endregion

    }
}
