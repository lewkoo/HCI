﻿namespace HCI
{
    partial class MovieDisplay
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ratingGauge = new System.Windows.Forms.ProgressBar();
            this.ratingLabel = new System.Windows.Forms.Label();
            this.length = new System.Windows.Forms.TextBox();
            this.lengthLabel = new System.Windows.Forms.Label();
            this.year = new System.Windows.Forms.TextBox();
            this.yearLaber = new System.Windows.Forms.Label();
            this.titleLabel = new System.Windows.Forms.Label();
            this.movieTitle = new System.Windows.Forms.TextBox();
            this.ratingInfo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ratingGauge
            // 
            this.ratingGauge.Location = new System.Drawing.Point(58, 84);
            this.ratingGauge.Maximum = 9;
            this.ratingGauge.Name = "ratingGauge";
            this.ratingGauge.Size = new System.Drawing.Size(85, 13);
            this.ratingGauge.Step = 1;
            this.ratingGauge.TabIndex = 15;
            // 
            // ratingLabel
            // 
            this.ratingLabel.AutoSize = true;
            this.ratingLabel.Location = new System.Drawing.Point(9, 84);
            this.ratingLabel.Name = "ratingLabel";
            this.ratingLabel.Size = new System.Drawing.Size(41, 13);
            this.ratingLabel.TabIndex = 14;
            this.ratingLabel.Text = "Rating:";
            // 
            // length
            // 
            this.length.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.length.Location = new System.Drawing.Point(58, 55);
            this.length.Name = "length";
            this.length.ReadOnly = true;
            this.length.Size = new System.Drawing.Size(116, 20);
            this.length.TabIndex = 13;
            // 
            // lengthLabel
            // 
            this.lengthLabel.AutoSize = true;
            this.lengthLabel.Location = new System.Drawing.Point(9, 58);
            this.lengthLabel.Name = "lengthLabel";
            this.lengthLabel.Size = new System.Drawing.Size(43, 13);
            this.lengthLabel.TabIndex = 12;
            this.lengthLabel.Text = "Length:";
            // 
            // year
            // 
            this.year.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.year.Location = new System.Drawing.Point(45, 29);
            this.year.Name = "year";
            this.year.ReadOnly = true;
            this.year.Size = new System.Drawing.Size(129, 20);
            this.year.TabIndex = 11;
            // 
            // yearLaber
            // 
            this.yearLaber.AutoSize = true;
            this.yearLaber.Location = new System.Drawing.Point(9, 32);
            this.yearLaber.Name = "yearLaber";
            this.yearLaber.Size = new System.Drawing.Size(32, 13);
            this.yearLaber.TabIndex = 10;
            this.yearLaber.Text = "Year:";
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Location = new System.Drawing.Point(9, 6);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(30, 13);
            this.titleLabel.TabIndex = 9;
            this.titleLabel.Text = "Title:";
            // 
            // movieTitle
            // 
            this.movieTitle.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.movieTitle.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.movieTitle.Location = new System.Drawing.Point(45, 3);
            this.movieTitle.Name = "movieTitle";
            this.movieTitle.ReadOnly = true;
            this.movieTitle.Size = new System.Drawing.Size(129, 20);
            this.movieTitle.TabIndex = 8;
            // 
            // ratingInfo
            // 
            this.ratingInfo.AutoSize = true;
            this.ratingInfo.Location = new System.Drawing.Point(150, 82);
            this.ratingInfo.Name = "ratingInfo";
            this.ratingInfo.Size = new System.Drawing.Size(0, 13);
            this.ratingInfo.TabIndex = 16;
            // 
            // MovieDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.ratingInfo);
            this.Controls.Add(this.ratingGauge);
            this.Controls.Add(this.ratingLabel);
            this.Controls.Add(this.length);
            this.Controls.Add(this.lengthLabel);
            this.Controls.Add(this.year);
            this.Controls.Add(this.yearLaber);
            this.Controls.Add(this.titleLabel);
            this.Controls.Add(this.movieTitle);
            this.Name = "MovieDisplay";
            this.Size = new System.Drawing.Size(188, 114);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar ratingGauge;
        private System.Windows.Forms.Label ratingLabel;
        private System.Windows.Forms.TextBox length;
        private System.Windows.Forms.Label lengthLabel;
        private System.Windows.Forms.TextBox year;
        private System.Windows.Forms.Label yearLaber;
        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.TextBox movieTitle;
        private System.Windows.Forms.Label ratingInfo;
    }
}
