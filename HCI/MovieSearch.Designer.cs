﻿namespace HCI
{
    partial class MovieSearch
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.genre = new System.Windows.Forms.Label();
            this.actors = new System.Windows.Forms.Label();
            this.genreBox = new System.Windows.Forms.TextBox();
            this.actorsBox = new System.Windows.Forms.TextBox();
            this.rating = new System.Windows.Forms.Label();
            this.ratingBar = new System.Windows.Forms.ProgressBar();
            this.director = new System.Windows.Forms.Label();
            this.length = new System.Windows.Forms.Label();
            this.year = new System.Windows.Forms.Label();
            this.titleBox = new System.Windows.Forms.TextBox();
            this.yearBox = new System.Windows.Forms.TextBox();
            this.lengthBox = new System.Windows.Forms.TextBox();
            this.directorBox = new System.Windows.Forms.TextBox();
            this.title = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // genre
            // 
            this.genre.AutoSize = true;
            this.genre.Location = new System.Drawing.Point(346, 46);
            this.genre.Name = "genre";
            this.genre.Size = new System.Drawing.Size(36, 13);
            this.genre.TabIndex = 27;
            this.genre.Text = "Genre";
            // 
            // actors
            // 
            this.actors.AutoSize = true;
            this.actors.Location = new System.Drawing.Point(168, 49);
            this.actors.Name = "actors";
            this.actors.Size = new System.Drawing.Size(37, 13);
            this.actors.TabIndex = 26;
            this.actors.Text = "Actors";
            // 
            // genreBox
            // 
            this.genreBox.Location = new System.Drawing.Point(346, 65);
            this.genreBox.Multiline = true;
            this.genreBox.Name = "genreBox";
            this.genreBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.genreBox.Size = new System.Drawing.Size(177, 100);
            this.genreBox.TabIndex = 25;
            // 
            // actorsBox
            // 
            this.actorsBox.Location = new System.Drawing.Point(168, 65);
            this.actorsBox.Multiline = true;
            this.actorsBox.Name = "actorsBox";
            this.actorsBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.actorsBox.Size = new System.Drawing.Size(172, 95);
            this.actorsBox.TabIndex = 24;
            // 
            // rating
            // 
            this.rating.AutoSize = true;
            this.rating.Location = new System.Drawing.Point(7, 147);
            this.rating.Name = "rating";
            this.rating.Size = new System.Drawing.Size(38, 13);
            this.rating.TabIndex = 23;
            this.rating.Text = "Rating";
            // 
            // ratingBar
            // 
            this.ratingBar.Location = new System.Drawing.Point(62, 144);
            this.ratingBar.Name = "ratingBar";
            this.ratingBar.Size = new System.Drawing.Size(100, 16);
            this.ratingBar.TabIndex = 22;
            // 
            // director
            // 
            this.director.AutoSize = true;
            this.director.Location = new System.Drawing.Point(7, 120);
            this.director.Name = "director";
            this.director.Size = new System.Drawing.Size(44, 13);
            this.director.TabIndex = 21;
            this.director.Text = "Director";
            // 
            // length
            // 
            this.length.AutoSize = true;
            this.length.Location = new System.Drawing.Point(7, 94);
            this.length.Name = "length";
            this.length.Size = new System.Drawing.Size(40, 13);
            this.length.TabIndex = 20;
            this.length.Text = "Length";
            // 
            // year
            // 
            this.year.AutoSize = true;
            this.year.Location = new System.Drawing.Point(7, 68);
            this.year.Name = "year";
            this.year.Size = new System.Drawing.Size(29, 13);
            this.year.TabIndex = 19;
            this.year.Text = "Year";
            // 
            // titleBox
            // 
            this.titleBox.Location = new System.Drawing.Point(62, 10);
            this.titleBox.Name = "titleBox";
            this.titleBox.Size = new System.Drawing.Size(461, 20);
            this.titleBox.TabIndex = 18;
            // 
            // yearBox
            // 
            this.yearBox.Location = new System.Drawing.Point(62, 65);
            this.yearBox.Name = "yearBox";
            this.yearBox.Size = new System.Drawing.Size(100, 20);
            this.yearBox.TabIndex = 17;
            // 
            // lengthBox
            // 
            this.lengthBox.Location = new System.Drawing.Point(62, 91);
            this.lengthBox.Name = "lengthBox";
            this.lengthBox.Size = new System.Drawing.Size(100, 20);
            this.lengthBox.TabIndex = 16;
            // 
            // directorBox
            // 
            this.directorBox.Location = new System.Drawing.Point(62, 117);
            this.directorBox.Name = "directorBox";
            this.directorBox.Size = new System.Drawing.Size(100, 20);
            this.directorBox.TabIndex = 15;
            // 
            // title
            // 
            this.title.AutoSize = true;
            this.title.Location = new System.Drawing.Point(7, 13);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(27, 13);
            this.title.TabIndex = 14;
            this.title.Text = "Title";
            // 
            // MovieSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.genre);
            this.Controls.Add(this.actors);
            this.Controls.Add(this.genreBox);
            this.Controls.Add(this.actorsBox);
            this.Controls.Add(this.rating);
            this.Controls.Add(this.ratingBar);
            this.Controls.Add(this.director);
            this.Controls.Add(this.length);
            this.Controls.Add(this.year);
            this.Controls.Add(this.titleBox);
            this.Controls.Add(this.yearBox);
            this.Controls.Add(this.lengthBox);
            this.Controls.Add(this.directorBox);
            this.Controls.Add(this.title);
            this.Name = "MovieSearch";
            this.Size = new System.Drawing.Size(725, 175);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label genre;
        private System.Windows.Forms.Label actors;
        private System.Windows.Forms.TextBox genreBox;
        private System.Windows.Forms.TextBox actorsBox;
        private System.Windows.Forms.Label rating;
        private System.Windows.Forms.ProgressBar ratingBar;
        private System.Windows.Forms.Label director;
        private System.Windows.Forms.Label length;
        private System.Windows.Forms.Label year;
        private System.Windows.Forms.TextBox titleBox;
        private System.Windows.Forms.TextBox yearBox;
        private System.Windows.Forms.TextBox lengthBox;
        private System.Windows.Forms.TextBox directorBox;
        private System.Windows.Forms.Label title;
    }
}
